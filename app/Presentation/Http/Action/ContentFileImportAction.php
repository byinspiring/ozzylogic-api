<?php

namespace App\Presentation\Http\Action;

use App\Application\Service\ParseContentFileService;
use App\Presentation\Http\Request\ContentFileImportRequest;
use Illuminate\Http\JsonResponse;

final class ContentFileImportAction extends BaseAction
{
    public function __construct(private readonly ParseContentFileService $parseContentFileService)
    {
    }

    public function run(ContentFileImportRequest $request): JsonResponse
    {
        $this->parseContentFileService->run(
            $request->post('contentType'),
            $request->file('contentFile')
        );

        return new JsonResponse(['code' => 200]);
    }
}
