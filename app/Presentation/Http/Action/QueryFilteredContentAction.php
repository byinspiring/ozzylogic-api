<?php

namespace App\Presentation\Http\Action;

use App\Application\DTO\QueryFilteredContentDTO;
use App\Application\Service\QueryFilteredContentService;
use App\Presentation\Http\Request\QueryFilteredContentRequest;
use Illuminate\Http\JsonResponse;

final class QueryFilteredContentAction extends BaseAction
{
    public function __construct(private readonly QueryFilteredContentService $service)
    {
    }

    public function run(QueryFilteredContentRequest $request): JsonResponse
    {
        $dto = QueryFilteredContentDTO::create(
            $request->get('contentType'),
            $request->get('perPage', 5000),
            $request->get('page', 1),
            $request->get('filters'),
            $request->get('orders')
        );

        return new JsonResponse([
            'code' => 200,
            'data' => $this->service->run($dto)
        ]);
    }
}
