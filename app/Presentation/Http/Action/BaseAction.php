<?php

namespace App\Presentation\Http\Action;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class BaseAction extends BaseController
{
    use AuthorizesRequests;
    use ValidatesRequests;
}
