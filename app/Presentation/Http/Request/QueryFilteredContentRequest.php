<?php

namespace App\Presentation\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

final class QueryFilteredContentRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'contentType' => ['required', 'string'],
            'filters' => ['sometimes', 'json'],
            'orderBy' => ['sometimes', 'json'],
            'perPage' => ['sometimes', 'int', 'max:5000'],
            'page' => ['sometimes', 'int', 'min:1']
        ];
    }
}
