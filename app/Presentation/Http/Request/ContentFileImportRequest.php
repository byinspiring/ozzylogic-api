<?php

namespace App\Presentation\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

final class ContentFileImportRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'contentType' => 'required|string',
            'contentFile' => [
                'required' ,
                File::types(['csv'])->min(1)->max(128 * 1024),
            ],
        ];
    }
}
