<?php

namespace App\Presentation\Http\Exception;

use Illuminate\Foundation\Exceptions\Handler as BaseExceptionHandler;
use Illuminate\Http\JsonResponse;
use Throwable;

class ExceptionHandler extends BaseExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param $request
     * @param Throwable $e
     * @return JsonResponse
     */
    public function render($request, Throwable $e): JsonResponse
    {
        return new JsonResponse(
            data: [
                'code' => $e->getCode() ?: 500,
                'message' => $e->getMessage()
            ],
            status: 200
        );
    }
}
