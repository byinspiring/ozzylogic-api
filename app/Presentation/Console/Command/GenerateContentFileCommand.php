<?php

namespace App\Presentation\Console\Command;

use App\Application\Exception\UnsupportedContentTypeException;
use App\Application\Service\GenerateContentFileService;
use App\Infrastructure\Exception\UnsupportedFileFormatException;
use App\Presentation\Console\Exception\InvalidArgumentProvidedException;
use App\Presentation\Console\Helper\CommandProgressBar;
use Exception;
use Illuminate\Console\Command;

class GenerateContentFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:generate-content-file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a file with test data according to provided amount of rows, format, content type.';

    public function __construct(
        private readonly GenerateContentFileService $service,
        private readonly int                        $maxRowsAmount,
        private readonly int                        $validationAttempts = 3,
        private int                                 $attempt = 0
    ) {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        try {
            $contentType = $this->askContentType();
            $rowsAmount = $this->askRowsAmount();
            $fileFormat = $this->askFileFormat();
            CommandProgressBar::create($this->output->createProgressBar($rowsAmount));
            $this->info('Processing...');
            $filePath = $this->service->run($contentType, $fileFormat, $rowsAmount);
            CommandProgressBar::finish();
            $this->info("\nEstimated time: " . CommandProgressBar::time() . ' seconds.');
            $this->info('Success.');
            $this->info('File path: ' . $filePath);
        } catch (InvalidArgumentProvidedException) {
            $this->error('I give up!');
        } catch (UnsupportedContentTypeException) {
            $this->error('You have provided an unsupported content type. Please, try another one.');
        } catch (UnsupportedFileFormatException) {
            $this->error('You have provided an unsupported file format. Please, try another one.');
        } catch (Exception) {
            $this->error('System failed.');
        }
    }

    /**
     * @return int
     * @throws InvalidArgumentProvidedException
     */
    private function askRowsAmount(): int
    {
        $rowsAmount = $this->ask('How many rows do you want to generate (limit - 9.9kk, didnt test more)?', '1000000');

        if ($this->validateRowsAmount($rowsAmount)) {
            return (int)$rowsAmount;
        } else {
            $this->info("Okay, let's try one more time...");
            $this->handle();
        }

        return 0;
    }

    /**
     * @param string $rowsAmount
     * @return bool
     * @throws InvalidArgumentProvidedException
     */
    private function validateRowsAmount(string $rowsAmount): bool
    {
        $isInt = !(filter_var($rowsAmount, FILTER_VALIDATE_INT) === false);

        if ($isInt && $this->maxRowsAmount > $rowsAmount) {
            return true;
        }

        if ($this->attempt < $this->validationAttempts) {
            $this->attempt++;

            return false;
        }

        throw new InvalidArgumentProvidedException();
    }

    /**
     * @return string
     */
    private function askFileFormat(): string
    {
        return $this->choice('What format type do you prefer?', ['csv'], 0, 3);
    }

    /**
     * @return string
     */
    private function askContentType(): string
    {
        return $this->choice('What type of data do you want?', ['users'], 0, 3);
    }
}
