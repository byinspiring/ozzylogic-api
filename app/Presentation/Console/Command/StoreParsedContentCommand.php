<?php

namespace App\Presentation\Console\Command;

use App\Application\Service\StoreParsedContentService;
use Exception;
use Illuminate\Console\Command;

class StoreParsedContentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:store-parsed-content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a file with test data according to provided amount of rows, format, content type.';

    public function __construct(private readonly StoreParsedContentService $service)
    {
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $this->service->run();
    }
}
