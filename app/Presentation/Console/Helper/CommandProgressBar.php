<?php

namespace App\Presentation\Console\Helper;

use Symfony\Component\Console\Helper\ProgressBar;

final class CommandProgressBar
{
    private static ?ProgressBar $progressBar = null;

    private function __construct()
    {
    }

    private function __clone(): void
    {
    }

    /**
     * @param ProgressBar $progressBar
     * @return void
     */
    public static function create(ProgressBar $progressBar): void
    {
        if (empty(self::$progressBar)) {
            self::$progressBar = $progressBar;
        }
    }

    /**
     * @return void
     */
    public static function start(): void
    {
        self::$progressBar->start();
    }

    /**
     * @param int $step
     * @return void
     */
    public static function advance(int $step): void
    {
        self::$progressBar->advance($step);
    }

    /**
     * @return void
     */
    public static function finish(): void
    {
        self::$progressBar->finish();
    }

    /**
     * @return float
     */
    public static function time(): float
    {
        return self::$progressBar->getEstimated();
    }
}
