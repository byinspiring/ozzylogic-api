<?php

namespace App\Presentation\Console\Exception;

use Exception;

final class InvalidArgumentProvidedException extends Exception
{
}
