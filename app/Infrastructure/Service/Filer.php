<?php

namespace App\Infrastructure\Service;

interface Filer
{
    public const READ_MODE = 'r';

    /**
     * @param string $contentType
     * @param string $format
     * @return string
     */
    public function generateFilePath(string $contentType, string $format): string;

    /**
     * @param string $filePath
     * @param string $mode
     * @return Filer
     */
    public function openFile(string $filePath, string $mode = 'a+'): Filer;

    /**
     * @return void
     */
    public function closeFile(): void;

    /**
     * @return bool
     */
    public function isEndOfFile(): bool;

    /**
     * @return string
     */
    public function readContent(): string;

    /**
     * @return array
     */
    public function readContentSeparated(): array;

    /**
     * @param string $format
     * @param array $data
     * @param bool $lastBatch
     * @return void
     */
    public function writeBatchContent(string $format, array $data, bool $lastBatch = false): void;
}
