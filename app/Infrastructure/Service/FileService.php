<?php

namespace App\Infrastructure\Service;

use App\Infrastructure\Exception\FileStreamHasBeenAlreadyOpenedException;
use App\Infrastructure\Exception\FileStreamHasNotBeenOpenedYetException;
use App\Infrastructure\Exception\UnsupportedFileFormatException;
use Illuminate\Support\Facades\File;
use SplFileObject;

final class FileService implements Filer
{
    private ?SplFileObject $file = null;

    public function __destruct()
    {
        $this->closeFile();
    }

    /**
     * @param string $contentType
     * @param string $format
     * @return string
     */
    public function generateFilePath(string $contentType, string $format): string
    {
        $path = storage_path('app') . '/files';

        if (!File::exists($path)) {
            File::makeDirectory($path);
        }

        return $path . '/' . $contentType . '-' . time() . '.' . $format;
    }

    /**
     * @param string $filePath
     * @param string $mode
     * @return Filer
     * @throws FileStreamHasBeenAlreadyOpenedException
     */
    public function openFile(string $filePath, string $mode = 'a+'): Filer
    {
        $this->fileShouldBeClosed();
        $this->file = new SplFileObject($filePath, $mode);

        return $this;
    }

    /**
     * @return void
     */
    public function closeFile(): void
    {
        unset($this->file);
    }

    /**
     * @return bool
     * @throws FileStreamHasNotBeenOpenedYetException
     */
    public function isEndOfFile(): bool
    {
        $this->fileShouldBeOpened();

        return $this->file->eof();
    }

    /**
     * @return string
     * @throws FileStreamHasNotBeenOpenedYetException
     */
    public function readContent(): string
    {
        $this->fileShouldBeOpened();

        return rtrim($this->file->fgets() ?: '', "\r\n");
    }

    /**
     * @return array
     * @throws FileStreamHasNotBeenOpenedYetException
     */
    public function readContentSeparated(): array
    {
        $this->fileShouldBeOpened();
        $content = $this->readContent();

        return !empty($content) ? explode(',', $content) : [];
    }

    /**
     * @param string $format
     * @param array $data
     * @param bool $lastBatch
     * @return void
     * @throws FileStreamHasNotBeenOpenedYetException
     * @throws UnsupportedFileFormatException
     */
    public function writeBatchContent(string $format, array $data, bool $lastBatch = false): void
    {
        $this->fileShouldBeOpened();

        match ($format) {
            'csv' => $this->writeBatchContentCsv($data, $lastBatch),

            default => throw new UnsupportedFileFormatException()
        };
    }

    /**
     * @param array $data
     * @param bool $lastBatch
     * @return void
     */
    private function writeBatchContentCsv(array $data, bool $lastBatch = false): void
    {
        $csvContent = implode(PHP_EOL, array_map(function ($row) {
            return implode(',', $row);
        }, $data));

        if (!$lastBatch) {
            $csvContent .= PHP_EOL;
        }

        $this->file->fwrite($csvContent);
    }

    /**
     * @return void
     * @throws FileStreamHasNotBeenOpenedYetException
     */
    private function fileShouldBeOpened(): void
    {
        if (empty($this->file)) {
            throw new FileStreamHasNotBeenOpenedYetException();
        }
    }

    /**
     * @return void
     * @throws FileStreamHasBeenAlreadyOpenedException
     */
    private function fileShouldBeClosed(): void
    {
        if (!empty($this->file)) {
            throw new FileStreamHasBeenAlreadyOpenedException();
        }
    }
}
