<?php

namespace App\Infrastructure\Queue;

use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMQ implements Queue
{
    public function __construct(
        private readonly AMQPStreamConnection $connection
    ) {
    }

    /**
     * @param callable $callback
     * @param string $queue
     * @param int $messagesAmount
     * @return void
     * @throws Exception
     */
    public function consume(
        callable $callback,
        string $queue,
        int $messagesAmount = 25
    ): void {
        $count = 1;
        $channel = $this->getChannel($queue);

        while ($message = $channel->basic_get(queue: $queue)) {
            if ($messagesAmount < $count) {
                break;
            }

            $callback(RabbitMQMessage::create($message));
            $count++;
        }

        $channel->close();
        $this->connection->close();
    }

    /**
     * @param string $queue
     * @param string $message
     * @return void
     */
    public function publish(string $queue, string $message): void
    {
        $channel = $this->getChannel($queue);
        $channel->exchange_declare($queue, 'topic', false, true, false);
        $channel->basic_publish(new AMQPMessage($message), $queue);
    }

    /**
     * @param string $queue
     * @return AMQPChannel
     */
    private function getChannel(string $queue): AMQPChannel
    {
        $channel = $this->connection->channel();
        $channel->exchange_declare($queue, type: 'topic', durable: true, auto_delete: false);
        $channel->queue_declare($queue, durable: true, auto_delete: false);
        $channel->queue_bind($queue, $queue);

        return $channel;
    }
}
