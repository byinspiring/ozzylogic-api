<?php

namespace App\Infrastructure\Queue;

interface Message
{
    /**
     * @return array
     */
    public function getBody(): array;

    /**
     * @return void
     */
    public function ack(): void;

    /**
     * @return void
     */
    public function reject(): void;
}
