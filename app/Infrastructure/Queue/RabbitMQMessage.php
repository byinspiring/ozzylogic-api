<?php

namespace App\Infrastructure\Queue;

use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMQMessage implements Message
{
    private function __construct(private readonly AMQPMessage $amqpMessage)
    {
    }

    /**
     * @param AMQPMessage $message
     * @return Message
     */
    public static function create(AMQPMessage $message): Message
    {
        return new self($message);
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        $message = $this->amqpMessage->getBody();

        return json_decode($message, true);
    }

    /**
     * @return void
     */
    public function ack(): void
    {
        $this->amqpMessage->ack();
    }

    /**
     * @return void
     */
    public function reject(): void
    {
        $this->amqpMessage->reject();
    }
}
