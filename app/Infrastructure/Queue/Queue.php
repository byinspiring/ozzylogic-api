<?php

namespace App\Infrastructure\Queue;

interface Queue
{
    /**
     * @param callable $callback
     * @param string $queue
     * @param int $messagesAmount
     * @return void
     */
    public function consume(callable $callback, string $queue, int $messagesAmount = 10): void;

    /**
     * @param string $queue
     * @param string $message
     * @return void
     */
    public function publish(string $queue, string $message): void;
}
