<?php

namespace App\Infrastructure\Cache;

use DateInterval;
use Psr\SimpleCache\InvalidArgumentException;
use Redis as RedisCore;

final class Redis implements Cache
{
    public function __construct(private readonly RedisCore $redis)
    {
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return (bool)$this->redis->exists($key);
    }

    /**
     * @param iterable $keys
     * @param mixed|null $default
     * @return iterable
     */
    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        try {
            $data = [];

            foreach ($keys as $key) {
                if (!$this->has($key)) {
                    continue;
                }

                $data[] = $this->get($key);
            }
        } catch (InvalidArgumentException) {
        }

        return $data;
    }

    /**
     * @param iterable $keys
     * @return bool
     */
    public function deleteMultiple(iterable $keys): bool
    {
        foreach ($keys as $key) {
            $this->delete($key);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function delete(string $key): bool
    {
        $this->redis->del($key);

        return true;
    }

    /**
     * @param iterable $values
     * @param DateInterval|int|null $ttl
     * @return bool
     */
    public function setMultiple(iterable $values, DateInterval|int|null $ttl = null): bool
    {
        try {
            foreach ($values as $key => $value) {
                $this->set($key, $value, $ttl);
            }

            return true;
        } catch (InvalidArgumentException) {
            return false;
        }
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, mixed $default = null): mixed
    {
        return $this->redis->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param DateInterval|int|null $ttl
     * @return bool
     */
    public function set(string $key, mixed $value, DateInterval|int|null $ttl = null): bool
    {
        try {
            if ($ttl instanceof DateInterval) {
                $ttl = $ttl->s;
            } elseif (is_null($ttl)) {
                $ttl = 60;
            }

            $this->redis->set($key, $value, $ttl);

            return true;
        } catch (InvalidArgumentException) {
            return false;
        }
    }
}
