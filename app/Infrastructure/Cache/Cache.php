<?php

namespace App\Infrastructure\Cache;

use Psr\SimpleCache\CacheInterface;

interface Cache extends CacheInterface
{
}
