<?php

namespace App\Infrastructure\Provider;

use App\Application\Generator\CreatingFileContentGenerator;
use App\Application\Generator\CreatingFileContentGeneratorContract;
use App\Application\Generator\ReadingFileContentGenerator;
use App\Application\Generator\ReadingFileContentGeneratorContract;
use App\Application\Helper\ContentFormatter;
use App\Application\Helper\ContentFormatterContract;
use App\Application\Helper\RowsBuffer;
use App\Application\Service\GenerateContentFileService;
use App\Application\Service\ParseContentFileService;
use App\Application\Service\StoreParsedContentService;
use App\Infrastructure\Cache\Cache;
use App\Infrastructure\Cache\Redis;
use App\Infrastructure\Middleware\CheckApiKeyMiddleware;
use App\Infrastructure\Persistence\BatchContentModelFactory;
use App\Infrastructure\Persistence\BatchContentModelFactoryContract;
use App\Infrastructure\Queue\Queue;
use App\Infrastructure\Queue\RabbitMQ;
use App\Infrastructure\Service\Filer;
use App\Infrastructure\Service\FileService;
use App\Presentation\Console\Command\GenerateContentFileCommand;
use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Redis as RedisCore;

class AppServiceProvider extends ServiceProvider
{
    private readonly array $rabbitConfig;
    private readonly array $fileGenerationConfig;
    private readonly array $authConfig;
    private readonly array $cacheConfig;

    public function __construct($app)
    {
        parent::__construct($app);

        $this->rabbitConfig = config('queue.connections.rabbitMQ');
        $this->fileGenerationConfig = config('file-generation');
        $this->authConfig = config('auth');
        $this->cacheConfig = config('cache');
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->registerInfrastructureLayer();
        $this->registerApplicationLayer();
        $this->registerPresentationLayer();
    }

    /**
     * Registering infrastructure.
     *
     * @return void
     */
    private function registerInfrastructureLayer(): void
    {
        $this->app->bind(AMQPStreamConnection::class, fn () => new AMQPStreamConnection(
            $this->rabbitConfig['host'],
            $this->rabbitConfig['port'],
            $this->rabbitConfig['user'],
            $this->rabbitConfig['password'],
            $this->rabbitConfig['vhost'],
        ));

        $redisConfig = $this->cacheConfig['stores']['redis'];

        $this->app->bind(RedisCore::class, function () use ($redisConfig) {
            $redis = new RedisCore();
            $redis->connect($redisConfig['host'], $redisConfig['port']);
            $redis->auth($redisConfig['password']);

            return $redis;
        });

        $this->app->bind(Cache::class, Redis::class);
        $this->app->bind(Queue::class, RabbitMQ::class);

        $this->app->bind(CheckApiKeyMiddleware::class, fn () => new CheckApiKeyMiddleware(
            $this->authConfig['apiKey']
        ));

        $this->app->bind(Filer::class, FileService::class);
        $this->app->bind(BatchContentModelFactoryContract::class, BatchContentModelFactory::class);
    }

    /**
     * Registering application.
     *
     * @return void
     */
    private function registerApplicationLayer(): void
    {
        $this->app->bind(CreatingFileContentGeneratorContract::class, fn () => new CreatingFileContentGenerator(
            $this->fileGenerationConfig['content']
        ));

        $this->app->bind(ReadingFileContentGeneratorContract::class, ReadingFileContentGenerator::class);

        $this->app->bind(ParseContentFileService::class, fn () => new ParseContentFileService(
            $this->app->make(ReadingFileContentGeneratorContract::class),
            $this->app->make(Filer::class),
            $this->app->make(Queue::class),
            $this->rabbitConfig['queues']['contentImport']
        ));

        $this->app->singleton(RowsBuffer::class);

        $this->app->bind(ContentFormatterContract::class, fn () => new ContentFormatter(
            $this->fileGenerationConfig['content']
        ));

        $this->app->bind(StoreParsedContentService::class, fn () => new StoreParsedContentService(
            $this->app->make(Queue::class),
            $this->app->make(ContentFormatterContract::class),
            $this->app->make(BatchContentModelFactoryContract::class),
            $this->rabbitConfig['queues']['contentImport']
        ));
    }

    /**
     * Registering presentation.
     *
     * @return void
     */
    private function registerPresentationLayer(): void
    {
        $this->app->bind(GenerateContentFileCommand::class, fn () => new GenerateContentFileCommand(
            $this->app->make(GenerateContentFileService::class),
            $this->fileGenerationConfig['maxRowsAmount']
        ));
    }
}
