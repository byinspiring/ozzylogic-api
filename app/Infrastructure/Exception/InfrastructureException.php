<?php

namespace App\Infrastructure\Exception;

use Exception;

abstract class InfrastructureException extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Infrastructure exception.';

    /**
     * @var int
     */
    protected $code = 500;
}
