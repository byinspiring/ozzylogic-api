<?php

namespace App\Infrastructure\Exception;

final class StoreParsedContentFailedException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'Some error during content store.';
}
