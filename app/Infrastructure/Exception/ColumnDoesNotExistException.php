<?php

namespace App\Infrastructure\Exception;

final class ColumnDoesNotExistException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'The column you are trying to use as a filter does not exist.';
}
