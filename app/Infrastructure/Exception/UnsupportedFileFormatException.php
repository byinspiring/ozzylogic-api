<?php

namespace App\Infrastructure\Exception;

final class UnsupportedFileFormatException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'Unsupported file format provided.';
}
