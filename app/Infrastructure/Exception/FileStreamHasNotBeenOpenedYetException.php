<?php

namespace App\Infrastructure\Exception;

final class FileStreamHasNotBeenOpenedYetException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'File stream has not been opened yet.';
}
