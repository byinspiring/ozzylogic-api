<?php

namespace App\Infrastructure\Exception;

final class FileStreamHasBeenAlreadyOpenedException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'File stream has been already opened.';
}
