<?php

namespace App\Infrastructure\Exception;

final class ContentTypePersistenceNotFoundException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'There is no persistence for such content type.';
}
