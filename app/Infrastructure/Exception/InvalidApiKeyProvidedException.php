<?php

namespace App\Infrastructure\Exception;

final class InvalidApiKeyProvidedException extends InfrastructureException
{
    /**
     * @var string
     */
    protected $message = 'Invalid api key provided.';

    /**
     * @var int
     */
    protected $code = 401;
}
