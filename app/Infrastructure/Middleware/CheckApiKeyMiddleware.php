<?php

namespace App\Infrastructure\Middleware;

use App\Infrastructure\Exception\InvalidApiKeyProvidedException;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class CheckApiKeyMiddleware
{
    public function __construct(private readonly string $apiKey)
    {
    }

    /**
     * @param Request $request
     * @param Closure $closure
     * @return JsonResponse
     * @throws InvalidApiKeyProvidedException
     */
    public function handle(Request $request, Closure $closure): JsonResponse
    {
        $incomingKey = $request->headers->get('X-API-KEY', '');

        if ($this->apiKey !== $incomingKey) {
            throw new InvalidApiKeyProvidedException();
        }

        return $closure($request);
    }
}
