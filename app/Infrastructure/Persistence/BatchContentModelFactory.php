<?php

namespace App\Infrastructure\Persistence;

use App\Infrastructure\Exception\ContentTypePersistenceNotFoundException;

final class BatchContentModelFactory implements BatchContentModelFactoryContract
{
    /**
     * @param string $contentType
     * @return BatchContent
     * @throws ContentTypePersistenceNotFoundException
     */
    public function create(string $contentType): BatchContent
    {
        return match ($contentType) {
            'users' => new Users(),

            default => throw new ContentTypePersistenceNotFoundException()
        };
    }
}
