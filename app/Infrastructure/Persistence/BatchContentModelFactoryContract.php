<?php

namespace App\Infrastructure\Persistence;

interface BatchContentModelFactoryContract
{
    /**
     * @param string $contentType
     * @return BatchContent
     */
    public function create(string $contentType): BatchContent;
}
