<?php

namespace App\Infrastructure\Persistence;

use App\Application\DTO\QueryFilteredContentDTO;
use App\Infrastructure\Exception\ColumnDoesNotExistException;
use App\Infrastructure\Exception\StoreParsedContentFailedException;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BatchContent extends Model
{
    /**
     * @param array $content
     * @return void
     * @throws StoreParsedContentFailedException
     */
    final public function import(array $content): void
    {
        try {
            DB::beginTransaction();
            static::query()->insert($content);
            DB::commit();
        } catch (Exception) {
            DB::rollBack();

            throw new StoreParsedContentFailedException();
        }
    }

    /**
     * @param QueryFilteredContentDTO $dto
     * @return array
     * @throws ColumnDoesNotExistException
     */
    final public function getFiltered(QueryFilteredContentDTO $dto): array
    {
        $query = static::query();

        foreach ($dto->getFiltersArray() as $field => $value) {
            if (!in_array($field, static::getColumns())) {
                throw new ColumnDoesNotExistException();
            }

            $query->where($field, '=', $value);
        }

        foreach ($dto->getOrdersArray() as $field => $direction) {
            if (!in_array($field, static::getColumns())) {
                throw new ColumnDoesNotExistException();
            }

            $query->orderBy($field, $direction);
        }

        $query->paginate(
            perPage: $dto->getPerPage(),
            page: $dto->getPage()
        );

        /** @var Builder $query */
        $query = $query->get();

        /** @var Collection $query */
        return $query->toArray();
    }

    /**
     * @return array
     */
    abstract protected function getColumns(): array;
}
