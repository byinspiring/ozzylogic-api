<?php

namespace App\Infrastructure\Persistence;

final class Users extends BatchContent
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @return string[]
     */
    protected function getColumns(): array
    {
        return [
            'id',
            'login',
            'mobile_number',
            'email',
            'city',
            'first_name',
            'last_name',
            'age',
            'gender',
            'salary',
            'car_model',
            'created_at',
            'updated_at'
        ];
    }
}
