<?php

namespace App\Application\Service;

use App\Application\Generator\ReadingFileContentGeneratorContract;
use App\Application\Helper\RowsBuffer;
use App\Infrastructure\Queue\Queue;
use App\Infrastructure\Service\Filer;
use Illuminate\Http\UploadedFile;

final class ParseContentFileService
{
    public function __construct(
        private readonly ReadingFileContentGeneratorContract $generator,
        private readonly Filer $filer,
        private readonly Queue $publisher,
        private readonly string $queue
    ) {
    }

    /**
     * @param string $contentType
     * @param UploadedFile $file
     * @return void
     */
    public function run(string $contentType, UploadedFile $file): void
    {
        $this->filer->openFile($file->getRealPath(), Filer::READ_MODE);

        foreach ($this->generator->run($this->filer) as $row) {
            if (RowsBuffer::add($row)) {
                continue;
            }

            $this->processBatch($contentType);
            RowsBuffer::add($row);
        }

        if (!RowsBuffer::isEmpty()) {
            $this->publisher->publish($this->queue, RowsBuffer::toString($contentType));
        }
    }

    /**
     * @param string $contentType
     * @return void
     */
    private function processBatch(string $contentType): void
    {
        $this->publisher->publish($this->queue, RowsBuffer::toString($contentType));
        RowsBuffer::reset();
    }
}
