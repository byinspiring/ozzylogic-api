<?php

namespace App\Application\Service;

use App\Application\Generator\CreatingFileContentGeneratorContract;
use App\Application\Helper\RowsBuffer;
use App\Infrastructure\Service\Filer;
use App\Presentation\Console\Helper\CommandProgressBar;

final class GenerateContentFileService
{
    public function __construct(
        private readonly Filer $filer,
        private readonly CreatingFileContentGeneratorContract $generator
    ) {
    }

    /**
     * @param string $contentType
     * @param string $format
     * @param int $batchSize
     * @return string
     */
    public function run(string $contentType, string $format, int $batchSize): string
    {
        $filePath = $this->prepareFiler($contentType, $format);

        foreach ($this->generator->run($contentType, $batchSize) as $row) {
            CommandProgressBar::advance(1);

            if (RowsBuffer::add($row)) {
                continue;
            }

            $this->processBatch($format);
            RowsBuffer::add($row);
        }

        if (!RowsBuffer::isEmpty()) {
            $this->processBatch($format, true);
        }

        return $filePath;
    }

    /**
     * @param string $contentType
     * @param string $format
     * @return string
     */
    private function prepareFiler(string $contentType, string $format): string
    {
        $filePath = $this->filer->generateFilePath($contentType, $format);
        $this->filer->openFile($filePath);

        return $filePath;
    }

    /**
     * @param string $format
     * @param bool $lastBatch
     * @return void
     */
    private function processBatch(string $format, bool $lastBatch = false): void
    {
        $this->filer->writeBatchContent($format, RowsBuffer::toArray(), $lastBatch);
        RowsBuffer::reset();
    }
}
