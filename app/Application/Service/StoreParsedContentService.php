<?php

namespace App\Application\Service;

use App\Application\Helper\ContentFormatterContract;
use App\Application\Helper\RowsBuffer;
use App\Infrastructure\Exception\StoreParsedContentFailedException;
use App\Infrastructure\Persistence\BatchContent;
use App\Infrastructure\Persistence\BatchContentModelFactoryContract;
use App\Infrastructure\Queue\Message;
use App\Infrastructure\Queue\Queue;
use Exception;

final class StoreParsedContentService
{
    public function __construct(
        private readonly Queue $consumer,
        private readonly ContentFormatterContract $formatter,
        private readonly BatchContentModelFactoryContract $modelFactory,
        private readonly string $queue
    ) {
    }

    /**
     * @return void
     */
    public function run(): void
    {
        $this->consumer->consume($this->callback(), $this->queue);
    }

    /**
     * @return callable
     */
    private function callback(): callable
    {
        $formatter = $this->formatter;

        return function (Message $message) use ($formatter): void {
            try {
                $this->processMessageBody($message->getBody(), $formatter);
                $message->ack();
            } catch (Exception) {
                $message->reject();
            }
        };
    }

    /**
     * @param array $messageBody
     * @param ContentFormatterContract $formatter
     * @return void
     * @throws StoreParsedContentFailedException
     */
    private function processMessageBody(array $messageBody, ContentFormatterContract $formatter): void
    {
        $contentType = (string)array_key_first($messageBody);
        $model = $this->modelFactory->create($contentType);

        foreach ($messageBody[$contentType] as $row) {
            $mappedData = $formatter->format($row, $contentType);

            if (RowsBuffer::add($mappedData)) {
                continue;
            }

            $this->store($model);
            RowsBuffer::add($mappedData);
        }

        if (!RowsBuffer::isEmpty()) {
            $this->store($model);
        }
    }

    /**
     * @param BatchContent $contentModel
     * @return void
     * @throws StoreParsedContentFailedException
     */
    private function store(BatchContent $contentModel): void
    {
        $contentModel->import(RowsBuffer::toArray());
        RowsBuffer::reset();
    }
}
