<?php

namespace App\Application\Service;

use App\Application\DTO\QueryFilteredContentDTO;
use App\Application\Exception\SettingIntoCacheDoesNotWorkException;
use App\Infrastructure\Cache\Cache;
use App\Infrastructure\Exception\ColumnDoesNotExistException;
use App\Infrastructure\Persistence\BatchContentModelFactoryContract;
use Psr\SimpleCache\InvalidArgumentException;

final class QueryFilteredContentService
{
    public function __construct(
        private readonly Cache $cache,
        private readonly BatchContentModelFactoryContract $modelFactory
    ) {
    }

    /**
     * @param QueryFilteredContentDTO $dto
     * @return array
     * @throws ColumnDoesNotExistException
     * @throws InvalidArgumentException
     * @throws SettingIntoCacheDoesNotWorkException
     */
    public function run(QueryFilteredContentDTO $dto): array
    {
        if ($this->cache->has($dto)) {
            $cache = $this->cache->get($dto);

            return json_decode($cache, true);
        }

        return $this->performQuery($dto);
    }

    /**
     * @param QueryFilteredContentDTO $dto
     * @return array
     * @throws ColumnDoesNotExistException
     * @throws InvalidArgumentException
     * @throws SettingIntoCacheDoesNotWorkException
     */
    private function performQuery(QueryFilteredContentDTO $dto): array
    {
        $data = $this->modelFactory->create($dto->getContentType())
            ->getFiltered($dto);

        if (!$this->cache->set($dto, json_encode($data), 300)) {
            throw new SettingIntoCacheDoesNotWorkException();
        }

        return $data;
    }
}
