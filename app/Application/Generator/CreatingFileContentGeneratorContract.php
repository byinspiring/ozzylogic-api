<?php

namespace App\Application\Generator;

use Generator;

interface CreatingFileContentGeneratorContract
{
    /**
     * @param string $contentType
     * @param int $rowsAmount
     * @return Generator
     */
    public function run(string $contentType, int $rowsAmount): Generator;
}
