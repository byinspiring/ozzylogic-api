<?php

namespace App\Application\Generator;

use App\Application\Exception\UnsupportedContentTypeException;
use Exception;
use Generator;

final class CreatingFileContentGenerator implements CreatingFileContentGeneratorContract
{
    public function __construct(private readonly array $config)
    {
    }

    /**
     * @param string $contentType
     * @param int $rowsAmount
     * @return Generator
     * @throws UnsupportedContentTypeException
     */
    public function run(string $contentType, int $rowsAmount): Generator
    {
        if (!isset($this->config[$contentType])) {
            throw new UnsupportedContentTypeException();
        }

        $config = $this->config[$contentType];

        for ($i = 0; $i < $rowsAmount; $i++) {
            yield $this->createRecord($config);
        }
    }

    /**
     * @param array $config
     * @return array
     * @throws UnsupportedContentTypeException
     */
    private function createRecord(array $config): array
    {
        $record = [];

        try {
            foreach ($config as $item) {
                if (is_array($item)) {
                    $record[] = $item[array_rand($item)];
                } else {
                    $record[] = $item;
                }
            }
        } catch (Exception) {
            throw new UnsupportedContentTypeException();
        }

        return $record;
    }
}
