<?php

namespace App\Application\Generator;

use App\Infrastructure\Service\Filer;
use Generator;

final class ReadingFileContentGenerator implements ReadingFileContentGeneratorContract
{
    /**
     * @param Filer $filer
     * @return Generator
     */
    public function run(Filer $filer): Generator
    {
        while (!$filer->isEndOfFile()) {
            yield $filer->readContentSeparated();
        }
    }
}
