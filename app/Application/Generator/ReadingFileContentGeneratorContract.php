<?php

namespace App\Application\Generator;

use App\Infrastructure\Service\Filer;
use Generator;

interface ReadingFileContentGeneratorContract
{
    /**
     * @param Filer $filer
     * @return Generator
     */
    public function run(Filer $filer): Generator;
}
