<?php

namespace App\Application\Helper;

use App\Application\Exception\UnsupportedContentTypeException;

final class ContentFormatter implements ContentFormatterContract
{
    public function __construct(private readonly array $config)
    {
    }

    /**
     * @param array $data
     * @param mixed $contentType
     * @return array
     * @throws UnsupportedContentTypeException
     */
    public function format(array $data, mixed $contentType): array
    {
        if (!isset($this->config[$contentType])) {
            throw new UnsupportedContentTypeException();
        }

        return array_combine(array_keys($this->config[$contentType]), $data);
    }
}
