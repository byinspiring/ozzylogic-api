<?php

namespace App\Application\Helper;

interface ContentFormatterContract
{
    /**
     * @param array $data
     * @param mixed $contentType
     * @return array
     */
    public function format(array $data, mixed $contentType): array;
}
