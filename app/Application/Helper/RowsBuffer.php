<?php

namespace App\Application\Helper;

final class RowsBuffer
{
    private const BUFFER_SIZE = 5000;
    private static array $rows = [];

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @param array $content
     * @return bool
     */
    public static function add(array $content): bool
    {
        if (count(self::$rows) >= self::BUFFER_SIZE) {
            return false;
        }

        self::$rows[] = $content;

        return true;
    }

    /**
     * @return bool
     */
    public static function isEmpty(): bool
    {
        return empty(self::$rows);
    }

    /**
     * @return void
     */
    public static function reset(): void
    {
        self::$rows = [];
    }

    /**
     * @param string $contentType
     * @return string
     */
    public static function toString(string $contentType): string
    {
        $string = json_encode([$contentType => self::$rows]);

        return $string ?: '';
    }

    /**
     * @return array
     */
    public static function toArray(): array
    {
        return self::$rows;
    }
}
