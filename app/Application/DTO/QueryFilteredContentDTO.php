<?php

namespace App\Application\DTO;

final class QueryFilteredContentDTO
{
    private function __construct(
        private readonly string $contentType,
        private readonly int $perPage,
        private readonly int $page,
        private readonly ?string $filters,
        private readonly ?string $orders
    ) {
    }

    /**
     * @param string $contentType
     * @param int $perPage
     * @param int $page
     * @param string|null $filters
     * @param string|null $orders
     * @return static
     */
    public static function create(
        string $contentType,
        int $perPage,
        int $page,
        ?string $filters = null,
        ?string $orders = null
    ): self {
        return new self($contentType, $perPage, $page, $filters, $orders);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            'ContentType: %s, PerPage: %d, Page: %d, Filters: %s, Orders: %s',
            $this->contentType,
            $this->perPage,
            $this->page,
            $this->filters ?? 'null',
            $this->orders ?? 'null'
        );
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @return array
     */
    public function getFiltersArray(): array
    {
        if (empty($this->filters)) {
            return [];
        }

        $filters = json_decode($this->filters, true);

        return $this->parseFiltersAndOrders($filters);
    }

    /**
     * @return array
     */
    public function getOrdersArray(): array
    {
        if (empty($this->orders)) {
            return [];
        }

        $orders = json_decode($this->orders, true);

        return $this->parseFiltersAndOrders($orders);
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param array $data
     * @return array
     */
    private function parseFiltersAndOrders(array $data): array
    {
        $parsedFilters = [];

        foreach ($data as $item) {
            foreach ($item as $key => $value) {
                $parsedFilters[$key] = $value;
            }
        }

        return $parsedFilters;
    }
}
