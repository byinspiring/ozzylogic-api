<?php

namespace App\Application\Exception;

final class SettingIntoCacheDoesNotWorkException extends ApplicationException
{
    /**
     * @var string
     */
    protected $message = 'Set into cache does not work';
}
