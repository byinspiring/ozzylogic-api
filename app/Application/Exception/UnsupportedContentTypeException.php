<?php

namespace App\Application\Exception;

final class UnsupportedContentTypeException extends ApplicationException
{
    /**
     * @var string
     */
    protected $message = 'Something went wrong with content.';
}
