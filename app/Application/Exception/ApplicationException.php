<?php

namespace App\Application\Exception;

use Exception;

abstract class ApplicationException extends Exception
{
    /**
     * @var string
     */
    protected $message = 'Application exception.';

    /**
     * @var int
     */
    protected $code = 500;
}
