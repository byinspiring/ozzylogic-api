<?php

use App\Presentation\Http\Action\ContentFileImportAction;
use App\Presentation\Http\Action\QueryFilteredContentAction;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return new JsonResponse(data: ['status' => 'ok']);
});

Route::middleware('auth.checkApiKey')->group(function () {
    Route::get('/content', QueryFilteredContentAction::class . '@run');
    Route::post('/content', ContentFileImportAction::class . '@run');
});
