## NOTE

Третья часть задачи (API запрос на получение записей) - не совсем очевидна. Сделал, как сделал, как понял необходимость.
Условие не совсем понятное с точки зрения проектирования БД. Все зависит от частоты
тех, или иных запросов, полей, которые чаще участвуют в условиях (а не просто могут участвовать).
По этой причине, я посчитал, что в данном случае, оправдана денормализация и не стал делить на
отдельные таблицы. С индексами та же история - наверное, в жизненных условиях, мы бы считали, что
email/login, может даже mobile_number - это unique значения. В любом случае - оптимизация работы с
БД/проектирование архитектуры должно проводиться основываясь на потребностях, поэтому я не занимался
вопросами индексирования и т.п. поскольку данный формат данных - слишком далек от реальных бизнес-кейсов,
в колонках по 50 уникальных записей (кроме gender) - тут надо индексировать всю таблицу, но это будет только во вред.

В общем, сделал оптимизацию на уровне кода, с БД надо предметно смотреть исходя из реальных проблем производительности.

## Technology stack
- php 8.2;
- Laravel 10+;
- PostgreSQL;
- RabbitMQ;
- Redis;
- Docker.

## Build project

1. `docker compose up -d --build` - runs docker to create full application environment.
2. Wait ~ 10-15 sec, rabbit container needs more time to up. 
3. Inside php container `composer install` - to install all packages.
4. Inside php container `php artisan migrate` - to run database migrations.
5. You can generate your own file, it will take a seconds, but you can find it in the root dir `users-170919146.csv`

P.S. maybe it should be optimized/automated, but I think it's enough.

## API Routes Documentation

This part outlines the API routes and their functionalities.

### Route: POST /api/v1/content

#### Description:
This route is used to upload a file containing data and store it.

#### Request Headers:
- `X-API-KEY`: Access key to API.

#### Request Parameters:
- `contentFile`: The file to be uploaded.
- `contentType`: The type of data to be stored.

#### Response:
- `code`: The status of the upload process.

### Route: GET /api/v1/content

#### Description:
This route is used to query data from content table.

#### Request Headers:
- `X-API-KEY`: Access key to API.

#### Request Parameters:

- `contentType`: (Required) Specifies the content to be queried.
- `filters`: (Optional) Specifies filters for query.
- `orders`: (Optional) Specifies orders for query.
- `perPage`: (Optional) Pagination tool - limit of records on a single page.
- `page`: (Optional) Pagination tool - offset, from which we should start query.

#### Response:

- `code`: The status of the query.
- `data`: The queried data matching the specified criteria.

## Console commands

- `php artisan app:generate-content-file` - manual command to generate file with data.
- `php artisan app:store-parsed-content` - scheduled command to consume parsed data.

## Composer commands

### Unit Testing
`composer unit` - runs application tests. Config is set in `phpunit.xml`.

NOTE: не успел написать тесты, а лишь бы какие не стал оставлять.

### Code formatting
`composer pint` - runs a Laravel package to format code according to PSR-12. Config is set in `pint.json`.

### Code analysis
`composer phpstan` - runs a phpstan analyser to analyse code and mark down possible errors. Config is set in `phpstan.neon`.

