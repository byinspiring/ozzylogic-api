<?php

return [
    'maxRowsAmount' => env('MAX_FILE_ROWS_AMOUNT'),
    'content' => [
        'users' => [
            'first_name' => [
                'Alex', 'Anna', 'David', 'Emma', 'John', 'Olivia', 'Michael', 'Sophia', 'Daniel', 'Isabella', 'James', 'Emily', 'William', 'Charlotte', 'Benjamin', 'Amelia', 'Andrew', 'Mia', 'Joseph', 'Harper', 'Alexander', 'Evelyn', 'Matthew', 'Abigail', 'Daniel', 'Avery', 'Henry', 'Sofia', 'Jacob', 'Ella', 'Ethan', 'Grace', 'Christopher', 'Chloe', 'Joshua', 'Lily', 'Gabriel', 'Mila', 'Nicholas', 'Natalie', 'Samuel', 'Eleanor', 'Ryan', 'Hannah', 'Nathan', 'Victoria', 'Matthew', 'Elizabeth'
            ],
            'last_name' => [
                'Smith', 'Johnson', 'Williams', 'Brown', 'Jones', 'Garcia', 'Miller', 'Davis', 'Rodriguez', 'Martinez', 'Hernandez', 'Lopez', 'Gonzalez', 'Wilson', 'Anderson', 'Thomas', 'Taylor', 'Moore', 'Jackson', 'Martin', 'Lee', 'Perez', 'Thompson', 'White', 'Harris', 'Sanchez', 'Clark', 'Ramirez', 'Lewis', 'Robinson', 'Walker', 'Young', 'Hall', 'Allen', 'King', 'Wright', 'Scott', 'Torres', 'Nguyen', 'Hill', 'Flores', 'Green', 'Adams', 'Nelson', 'Baker', 'Hall', 'Rivera'
            ],
            'age' => range(18, 60),
            'gender' => ['M', 'F'],
            'mobile_number' => [
                '555-123-4567', '555-987-6543', '555-456-7890', '555-890-1234', '555-234-5678', '555-678-9012', '555-345-6789', '555-789-0123', '555-901-2345', '555-432-1098', '555-765-4321', '555-210-9876', '555-876-5432', '555-543-2109', '555-987-6543', '555-123-4567', '555-456-7890', '555-890-1234', '555-234-5678', '555-678-9012', '555-345-6789', '555-789-0123', '555-901-2345', '555-432-1098', '555-765-4321', '555-210-9876', '555-876-5432', '555-543-2109', '555-987-6543', '555-123-4567', '555-456-7890', '555-890-1234', '555-234-5678', '555-678-9012', '555-345-6789', '555-789-0123', '555-901-2345', '555-432-1098', '555-765-4321', '555-210-9876', '555-876-5432', '555-543-2109', '555-987-6543', '555-123-4567', '555-456-7890', '555-890-1234', '555-234-5678'
            ],
            'email' => [
                'xqhyr@gmail.com', 'vbgka@gmail.com', 'kwumd@gmail.com', 'zdcri@gmail.com', 'afhsj@gmail.com', 'lnxzu@gmail.com', 'eqvwd@gmail.com', 'gjroa@gmail.com', 'cwjfo@gmail.com', 'oiprx@gmail.com', 'imlzv@gmail.com', 'bopqk@gmail.com', 'pjuna@gmail.com', 'fnvkx@gmail.com', 'sebua@gmail.com', 'mgkiv@gmail.com', 'xtlme@gmail.com', 'djfhs@gmail.com', 'zqfwi@gmail.com', 'yrwvk@gmail.com', 'hpzkb@gmail.com', 'roiwv@gmail.com', 'ybdsr@gmail.com', 'kayns@gmail.com', 'jxnsb@gmail.com', 'wdiyx@gmail.com'
            ],
            'city' => [
                'New York', 'Los Angeles', 'Chicago', 'Houston', 'Philadelphia', 'Phoenix', 'San Antonio', 'San Diego', 'Dallas', 'San Jose', 'Austin', 'Jacksonville', 'San Francisco', 'Indianapolis', 'Columbus', 'Fort Worth', 'Charlotte', 'Detroit', 'El Paso', 'Memphis', 'Seattle', 'Denver', 'Washington', 'Boston', 'El Paso', 'Nashville', 'Oklahoma City', 'Las Vegas', 'Baltimore', 'Louisville', 'Milwaukee', 'Albuquerque', 'Tucson', 'Fresno', 'Mesa', 'Sacramento', 'Atlanta', 'Kansas City', 'Omaha', 'Raleigh', 'Miami', 'Long Beach', 'Virginia Beach', 'Oakland', 'Minneapolis', 'Tampa', 'Tulsa', 'Arlington', 'New Orleans', 'Wichita'
            ],
            'login' => [
                'bluebird123', 'sunset88', 'greenapple', 'silvermoon', 'happydog12', 'oceanwave', 'purplecat', 'mountainview', 'luckycharm', 'redrose123', 'beachlover', 'winterstorm', 'fastcar', 'bigapple', 'sunnyday', 'coffeelover', 'whitelily', 'blacksheep', 'moonlight', 'foggyday', 'windyhill', 'nightowl', 'earlybird', 'hotcoffee', 'sweetdream', 'blackcat', 'darknight', 'cloudyday', 'birdsong', 'rainbow123', 'snowwhite', 'sunflower', 'greentree', 'purpleiris', 'blueocean', 'happyfrog', 'lazycat', 'wildhorse', 'bravesoul', 'breezylane', 'brightstar', 'cosmicray', 'crimsonrose', 'dancingwind', 'dazzlingsun', 'deepsea', 'distantmoon', 'dreamcatcher', 'echoingwind'
            ],
            'car_model' => [
                'Accord', 'Camry', 'Civic', 'Corolla', 'F-150', 'CR-V', 'Escape', 'Fusion', 'Highlander', 'Wrangler', 'Grand Cherokee', 'Tacoma', 'Explorer', 'RAV4', 'Altima', 'Tacoma', 'Malibu', 'Equinox', 'Sentra', 'Focus', 'Tahoe', 'Cherokee', 'Mustang', 'Odyssey', 'Pilot', 'Sierra', 'Sonata', 'Versa', 'Impala', 'Jetta', 'Highlander', 'Accord', 'Expedition', 'Traverse', '4Runner', 'Tucson', 'Optima', 'Explorer', 'Camry', 'Pacifica', 'Edge', 'Rogue', 'Cruze', 'Silverado', 'Tucson', 'Elantra', 'Tundra', 'Tacoma', 'Outback', 'Ascent'
            ],
            'salary' => range(1000, 5000)
        ]
    ]
];
